# librerie e altri esempi
library(here)

# https://dati.comune.milano.it/
opendatafile=here(
  "data","opendataMilano",
  "ds203_sociale_pazienti_prestazioni_pronto_soccorso_struttura_sanitaria_2007-2013_.csv"
  )
opendata_df <- read.csv(opendatafile,sep = ";")
head(opendata_df)


library(tidyverse)
# documentazione tidyverse
# https://tidyverse.tidyverse.org/

# libri per approfondire:
# https://r4ds.had.co.nz/introduction.html

tibble(campione=rep(c("A","B","C"),each=3),risultato=runif(9))

mtcars

mtcars_tibble <- mtcars %>%
  as_tibble(.,rownames = "car")

glimpse(mtcars_tibble)

colonne <- c("car","mpg","cyl","wt","am","hp")
# plot
mtcars_tidy <- mtcars %>%
  as_tibble(rownames = "car") %>%
  filter(am==1) %>%
  #select(!!colonne) %>%
  dplyr::select(car,mpg,cyl,wt,am,hp) %>%
  rename(cylinder=cyl,weight=wt,transmission=am) %>%
  arrange(cylinder,desc(mpg))


p<-ggplot(mtcars_tidy, aes(x=mpg,y=weight)) +
  geom_point()

ggsave(plot=p,filename = here("data","grafico_mpg-weight.png"),
       width = 12,height = 6,units = "cm")

ggplot(mtcars_tidy, aes(x=as.factor(cylinder),y=hp,fill=as.factor(cylinder))) +
  geom_boxplot() 


ggplot(starwars, aes(x=sex, y=height,fill=eye_color)) +
  geom_boxplot()

?ggplot

#opendata
opendata_df <- read_delim(opendatafile,delim = ";")

unique(opendata_df$Denominazione)


opendata_df








##
opendata_df %>%
  filter(Denominazione=="Istituto Ortopedico G. Pini") %>%
  pivot_longer(-c(Anno,`Struttura Ospedaliera`,Denominazione),
               names_to = "condizione",values_to = "valore") %>%
      mutate(valore=as.character(valore)) %>%
      mutate(valore=str_remove(valore,fixed("."))) %>%
      mutate(valore=as.numeric(valore)) %>%
      ggplot(aes(x=Anno,y=valore,fill=condizione)) +
      geom_col(position = "dodge") +
      theme_classic(base_size = 16)




lista$vetB <- c(4,5,7,8)

map2(lista1,lista2, ~ {.x+.y+1
  })


lista_grafici <- opendata_df %>%
  split(.,.$Denominazione) %>%
  imap(., ~ {
    pivot_longer(.x, -c(Anno,`Struttura Ospedaliera`,Denominazione),
                 names_to = "condizione",values_to = "valore") %>%
      mutate(valore=as.character(valore)) %>%
      mutate(valore=str_remove(valore,fixed("."))) %>%
      mutate(valore=as.numeric(valore)) %>%
      ggplot(aes(x=Anno,y=valore,fill=condizione)) +
      geom_col(position = "dodge",color="black") +
      ggtitle(.y) +
      theme_classic(base_size = 16)
  })
  
lista_grafici[[4]]

imap(lista_grafici,~ ggsave(plot=.x,filename = here("data",paste0("grafico_ps",.y,".png"))
                            ,width = 12,height = 6,units = "cm"))
